from django.contrib import admin
from django.urls import path
from .views import SongListView, SongDetailView, SongUpdateView, search_songs, SongCreateView, SongDeleteView

app_name = "songs"

urlpatterns = [
    path('', SongListView.as_view(), name="list"),
    path('create/', SongCreateView.as_view(), name='create'),
    path('<int:pk>/', SongDetailView.as_view(), name='detail'),
    path('<int:pk>/update/', SongUpdateView.as_view(), name='update'),
    path('search/', search_songs, name='search'),
    path('delete/', SongDeleteView, name='delete')
]
