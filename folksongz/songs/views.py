from django.shortcuts import render
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from .models import Song
from django.urls import reverse_lazy
from django.db.models import Q
from common.turbo.response import TurboStreamTemplateResponse
from django.forms import ModelForm
from urllib import parse
# Create your views here.

WIDGET_BASE_CLASSES = "rounded-xl px-4 py-1"

class SongForm(ModelForm):
    class Meta:
        model = Song
        fields = ["title", "lyrics"]

        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({
            'class': f"rounded-full {WIDGET_BASE_CLASSES}",
            'placeholder': 'Song Title',
        })
        self.fields["lyrics"].widget.attrs.update({
            'class': f"{WIDGET_BASE_CLASSES}",
            'placeholder': 'Lyrics',
        })

        
def search_songs_query(search):
    return Song.objects.filter(Q(title__contains=search) | Q(lyrics__contains=search))


class SongDeleteView(DeleteView):
    model = Song
    success_url = reverse_lazy("songs:list")

class SongCreateView(CreateView):
    model = Song
    form_class = SongForm
    template_name_suffix = "_create"
    success_url = reverse_lazy("songs:detail")
    
    def get_success_url(self):
        success_url = self.request.GET.get('success_url')
        if success_url is None:
            if self.object:
                return self.object.get_absolute_url()
            else:
                return None
        else:
            return success_url
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['target'] = self.request.GET.get('target')
        context['success_url'] = self.get_success_url()
        return context


class SongListView(ListView):
    model = Song
    context_object_name = "songs_list"

    def get_search(self):
        return self.request.GET.get('search')
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search = self.get_search()
        if search is not None:
            context['search'] = search

        # Adding a bunch of stuff to allow for collapsable inline create

        # create form should redirect back to existing page on success 
        create_success_url = parse.quote(f"{reverse_lazy('songs:list')}")
        # Target needs to be main-content so create response will replace list and collapse input
        qs = f"?success_url={create_success_url}&target=main-content"

        # existing query string should be maintainted
        if self.request.META["QUERY_STRING"] is not None:
            qs += f"&{self.request.META['QUERY_STRING']}"
            
        context["create_song_link_url"] = f"{reverse_lazy('songs:create')}{qs}"
        context['create_form'] = SongForm()
        return context

    
    def get_queryset(self):
        search = self.get_search()
        if search is None:
            qs = Song.objects.all()
        else:
            qs = search_songs_query(search)

        return qs.order_by("title")

        
class SongDetailView(DetailView):
    model = Song
    context_object_name = "song"


class SongUpdateView(UpdateView):
    model = Song
    form_class = SongForm
    


def search_songs(request):
    search = request.GET.get("search")
    
    songs = search_songs_query(search)

    return TurboStreamTemplateResponse(
        request,
        "songs/search/_results.html",
        {"results": songs, "search": search },
        target="search-results",
        action="update"
    )
