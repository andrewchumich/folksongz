from django.db import models
from django.urls import reverse


# Create your models here.
class Song(models.Model):
    title = models.CharField(max_length=164, null=False, blank=False)
    lyrics = models.TextField(null=False, blank=False)

    def get_absolute_url(self):
        return reverse("songs:detail", args=[str(self.id)])

    def get_update_url(self):
        return reverse("songs:update", args=[str(self.id)])

    def get_list_url(self):
        return reverse("songs:list")

    def get_container_frame_id(self):
        return f"song_container_{self.id}"

    def __str__(self):
        return f"{self.title}"
